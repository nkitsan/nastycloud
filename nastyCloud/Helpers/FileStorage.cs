﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nastyCloud.Helpers
{
    public class FileStorage
    {
        public string CustomAvatarsDirectory { get; set; }
        public string DefaultAvatarsDirectory { get; set; }
        public string FilesDirectory { get; set; }
        public string FileServer { get; set; }
    }
}
