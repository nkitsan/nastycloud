﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nastyCloud.TagHelpers
{
    public class AuthorTagHelper : TagHelper
    {
        private const string address = "https://github.com/nkitsan";
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";
            output.Attributes.SetAttribute("href", address);
            output.Content.SetContent("Authors GitHub");
        }
    }
}
