﻿const deleteFilesRequest = (id) => {
    const url = "/api/files/" + id;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            let files = document.querySelectorAll(".file");
            for (let file of files) {
                if (file.dataset.id === id) {
                    file.remove();
                }
            }
        }
    };
    xhr.open("DELETE", url, true);
    xhr.send();
};

const deleteUsersRequest = (id) => {
    const url = "/api/users/" + id;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            let users = document.querySelectorAll(".user");
            for (let user of users) {
                if (user.dataset.id === id) {
                    user.remove();
                }
            }
        }
    };
    xhr.open("DELETE", url, true);
    xhr.send();
};


const changeAccessRequest = (id) => {
    const url = "/api/users/" + id + "/rights";
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            let users = document.querySelectorAll(".user");
            for (let user of users) {
                if (user.dataset.id === id) {
                    let button = user.querySelector(".change-role");
                    button.innerHTML = button.innerHTML == "To users" ? "To Admins" : "To users";
                }
            }
        }
    };
    xhr.open("PUT", url, true);
    xhr.send();
};

const clickHandler = (event) => {
    if (event.target.classList.contains("submit-delete-btn")) {
        deleteFilesRequest(event.target.dataset.id);
    }
    else if (event.target.classList.contains("change-role")) {
        changeAccessRequest(event.target.dataset.id);
    }
    else if (event.target.classList.contains("delete-user-btn")) {
        deleteUsersRequest(event.target.dataset.id);
    }
}

document.querySelector(".container").addEventListener("click", clickHandler);