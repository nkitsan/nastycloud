﻿const formModalLink = (id) => {
    let button = document.querySelector(".submit-generate-btn");
    button.dataset.id = id;
    let infoAboutLink = document.querySelector(".input-to-link");
    const url = "/api/files/" + id;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            infoAboutLink.value = JSON.parse(xhr.response)["sharingLink"];
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
};

const formModalAccess = (id) => {
    let button = document.querySelector(".submit-access-btn");
    button.dataset.id = id;
    let infoAboutMode = document.querySelector(".div-to-access");
    const url = "/api/files/" + id;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            const fileMode = JSON.parse(xhr.response)["isPublic"];
            if (fileMode) {
                infoAboutMode.innerText = "This file is public, press button to change mode";
            }
            else {
                infoAboutMode.innerText = "This file is private, press button to change mode";
            }
        }
    };
    xhr.open("GET", url, true);
    xhr.send();

};

const formModalChange = (id) => {
    let button = document.querySelector(".submit-edit-btn");
    button.dataset.id = id;
    let files = document.querySelectorAll(".file");
    let name = "";
    for (let file of files) {
        if (file.dataset.id === id) {
            name = file.querySelector(".file-name").innerText.split(".")[0];
        }
    }
    document.querySelector(".input-to-name").value = name;
};

const formModalDelete = (id) => {
    let button = document.querySelector(".submit-delete-btn");
    button.dataset.id = id;
};

const shareLinkRequest = () => {
    let infoAboutLink = document.querySelector(".input-to-link");
    let button = document.querySelector(".submit-generate-btn");
    const url = "/api/files/" + button.dataset.id + "/link";
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            infoAboutLink.value = JSON.parse(xhr.response)["sharingLink"];
        }
    };
    xhr.open("PUT", url, true);
    xhr.send();
};

const changeAccessRequest = () => {
    let button = document.querySelector(".submit-access-btn");
    const url = "/api/files/" + button.dataset.id + "/access";
    let xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);
    xhr.send();
};

const changeNameRequest = () => {
    let button = document.querySelector(".submit-edit-btn");
    const url = "/api/files/" + button.dataset.id + "/name";
    const name = document.querySelector(".input-to-name").value;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            let files = document.querySelectorAll(".file");
            for (let file of files) {
                if (file.dataset.id === button.dataset.id) {
                    file.querySelector(".file-name").innerText = JSON.parse(xhr.response)["clientName"];
                    break;
                }
            }
        }
    };
    let formData = new FormData();
    formData.append("name", name);
    xhr.open("PUT", url, true);
    xhr.send(formData);
};

const deleteRequest = () => {
    let button = document.querySelector(".submit-delete-btn");
    const url = "/api/files/" + button.dataset.id;
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.status === 200) {
            let files = document.querySelectorAll(".file");
            for (let file of files) {
                if (file.dataset.id === button.dataset.id) {
                    file.remove();
                }
            }
        }
    };
    xhr.open("DELETE", url, true);
    xhr.send();
};

const sendFile = () => {
    const input = document.querySelector(".custom-file-input");
    const files = input.files;
    const url = "/api/files";
    if (files.length !== 0) {
        let xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            let response = JSON.parse(xhr.response);
            console.log(response);
            let list = document.querySelector(".list-group");
            let newFile = "<div class=\"list-group-item d-flex justify-content-between bg-light mb-3 file\" data-id=" + response.id + "><span class=\"p-2\"><span class=\"file-name\">" + response.clientName + "</span></span><span class=\"p-2\"><button type=\"button\" class=\"btn btn-dark download-file\"><a href=\"/api/downloader/"
                + response.id + "\">Download</a></button><button type=\"button\" class=\"btn btn-dark\" data-toggle=\"dropdown\"> ... </button><ul class=\"dropdown-menu\"><li class=\"dropdown-item\"><a href=\"#shareViaLink\" class=\"link-to-share\" data-id=" + response.id + " data-toggle=\"modal\">Sharelink</a></li><li class=\"dropdown-item\"><a href=\"#setAccess\" class=\"link-to-access\" data-id=" + response.id + " data-toggle=\"modal\">Set access</a></li><li class=\"dropdown-item\"><a href=\"#rename\" class=\"link-to-rename\" data-id=" + response.id + " data-toggle=\"modal\">Rename</a></li><li class=\"dropdown-item\"><a href=\"#delete\" class=\"link-to-delete\" data-id=" + response.id + " data-toggle=\"modal\">Delete</a></li></ul></span></div>";
            list.innerHTML += newFile;
        }
        xhr.open("POST", url, true);
        let formData = new FormData();
        formData.append("file", files[0]);
        xhr.send(formData);
    }
};

const clickHandler = (event) => {
    if (event.target.classList.contains("link-to-share")) {
        formModalLink(event.target.dataset.id);
    }
    else if (event.target.classList.contains("link-to-access")) {
        formModalAccess(event.target.dataset.id);
    }
    else if (event.target.classList.contains("link-to-rename")) {
        formModalChange(event.target.dataset.id);
    }
    else if (event.target.classList.contains("link-to-delete")) {
        formModalDelete(event.target.dataset.id);
    }
    else if (event.target.classList.contains("submit-generate-btn")) {
        shareLinkRequest();
    }
    else if (event.target.classList.contains("submit-access-btn")) {
        changeAccessRequest();
    }
    else if (event.target.classList.contains("submit-edit-btn")) {
        changeNameRequest();
    }
    else if (event.target.classList.contains("submit-delete-btn")) {
        deleteRequest();
    }
}

document.querySelector(".container").addEventListener("click", clickHandler);
document.querySelector(".custom-file-input").addEventListener("change", sendFile);