﻿using System.ComponentModel.DataAnnotations;

namespace nastyCloud.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email not defined")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not defined")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}