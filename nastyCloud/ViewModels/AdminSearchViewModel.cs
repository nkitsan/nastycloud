﻿using System;
using System.Collections.Generic;
using nastyCloud.Models;

namespace nastyCloud.ViewModels
{
    public class AdminSearchViewModel
    {
        public List<User> Users { get; set; }
        public List<UserFile> UserFiles { get; set; }
    }
}
