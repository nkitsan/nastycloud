﻿using System.ComponentModel.DataAnnotations;

namespace nastyCloud.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Nickname not defined")]
        public string Nickname { get; set; }

        [Required(ErrorMessage = "Email not defined")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not defined")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password is incorrect")]
        public string ConfirmPassword { get; set; }
    }
}