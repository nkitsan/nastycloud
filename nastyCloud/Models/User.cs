﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nastyCloud.Models
{
    public class User
    {
        public int Id { get; set; }
        public bool isAdmin { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AvatarName { get; set; }
    }
}
