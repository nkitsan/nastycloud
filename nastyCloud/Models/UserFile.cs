﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nastyCloud.Models
{
    public class UserFile
    {
        public int Id { get; set; }
        public string ServerName { get; set; }
        public string ClientName { get; set; }
        public bool isPublic { get; set; }
        public string SharingLink { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
