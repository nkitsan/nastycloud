﻿using Microsoft.EntityFrameworkCore;

namespace nastyCloud.Models
{
    public class CloudContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserFile> UserFiles { get; set; }
        public CloudContext(DbContextOptions<CloudContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
