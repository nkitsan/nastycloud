﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nastyCloud.RouteConstraints
{
    public class HomeConstraint : IRouteConstraint
    {

        public bool Match(HttpContext httpContext, IRouter route, string routeKey,
        RouteValueDictionary values, RouteDirection routeDirection)
        {
            var routeValues = new[] { "Home", "Index" };
            var routeKeys = new[] { "id", "params" };

            foreach (var val in routeValues)
            {
                if (!values.Values.Contains(val))
                {
                    return true;
                }
            }

            foreach (var val in routeKeys)
            {
                if (values.Keys.Contains(val))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
