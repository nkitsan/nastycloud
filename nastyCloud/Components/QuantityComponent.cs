﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using nastyCloud.Models;

namespace nastyCloud.Components
{
    public class QuantityComponent : ViewComponent
    {
        public string Invoke(List<User> users, List<UserFile> files)
        {
            int fileCount = files?.Count ?? 0;
            int userCount = users?.Count ?? 0;

            if (userCount == 0 && fileCount == 0)
            {
                return $"No results was found";
            }

            if (userCount == 0 && fileCount != 0)
            {
                return $"{files.Count} files was found";
            }

            if (userCount != 0 && fileCount == 0)
            {
                return $"{users.Count} users was found";
            }

            return $"{users.Count} users and {files.Count} files was found";
        }
    }
}
