﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using nastyCloud.Models;
using nastyCloud.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace nastyCloud.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class DownloaderController : Controller
    {
        private CloudContext db;
        private readonly FileStorage fileStorage;

        public DownloaderController(CloudContext context, IOptions<FileStorage> fileStorage)
        {
            this.db = context;
            this.fileStorage = fileStorage.Value;
        }

        [HttpGet("{link}")]
        public async Task<IActionResult> Get(string link)
        {
            int fileId;
            UserFile requestFile;
            string path;
            string contentType;

            if (int.TryParse(link, out fileId))
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return BadRequest();
                }

                requestFile = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == fileId);
                User userIdentity = await db.Users.FirstOrDefaultAsync(user => user.Email == User.Identity.Name);

                if (requestFile.isPublic || userIdentity.isAdmin || userIdentity.Id == requestFile.UserId)
                {
                    path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.FilesDirectory, requestFile.ServerName);
                    contentType = MimeTypeMap.GetMimeType(Path.GetExtension(requestFile.ServerName.Trim('.')));
                    byte[] massive = System.IO.File.ReadAllBytes(path);
                    return File(massive, contentType, requestFile.ClientName);
                }

                return BadRequest();
            }

            requestFile = await db.UserFiles.FirstOrDefaultAsync(file => file.SharingLink.EndsWith(link));
            path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.FilesDirectory, requestFile.ServerName);
            contentType = MimeTypeMap.GetMimeType(Path.GetExtension(requestFile.ServerName.Trim('.')));
            byte[] mas = System.IO.File.ReadAllBytes(path);
            return File(mas, contentType, requestFile.ClientName);
        }

    }
}