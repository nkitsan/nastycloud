﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nastyCloud.Models;
using nastyCloud.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace nastyCloud.Controllers
{

    [Produces("application/json")]
    [Route("api/files")]
    public class FilesController : Controller
    {
        private CloudContext db;
        private readonly FileStorage fileStorage;

        public FilesController(CloudContext context, IOptions<FileStorage> fileStorage)
        {
            this.db = context;
            this.fileStorage = fileStorage.Value;
            
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            UserFile requestFile = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == id);
            return Ok(requestFile);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file)
        {
            var servername = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.FilesDirectory, servername);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            UserFile userFile = new UserFile
            {
                ServerName = servername,
                ClientName = file.FileName,
                isPublic = false,
                SharingLink = null,
                UserId = user.Id,
                User = user
            };
            db.UserFiles.Add(userFile);
            await db.SaveChangesAsync();
            return Redirect("files/" + userFile.Id.ToString());
        }

        [Authorize]
        [HttpPut("{id}/name")]
        public async Task<IActionResult> PutName(int id)
        {
            UserFile fileToChange = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == id);
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.Id == fileToChange.UserId)
            {
                var name = Request.Form["name"];
                if (name != "" && name != fileToChange.ClientName)
                {
                    fileToChange.ClientName = name + Path.GetExtension(fileToChange.ServerName);
                    await db.SaveChangesAsync();
                }
                return Ok(fileToChange);
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPut("{id}/access")]
        public async Task<IActionResult> PutAccess(int id)
        {
            UserFile fileToChange = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == id);
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.Id == fileToChange.UserId)
            {

                fileToChange.isPublic = fileToChange.isPublic ? false : true;
                await db.SaveChangesAsync();
                return Ok(fileToChange);
            }
            return BadRequest();
        }

        [Authorize]
        [HttpPut("{id}/link")]
        public async Task<IActionResult> PutLink(int id)
        {
            UserFile fileToChange = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == id);
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.Id == fileToChange.UserId)
            {
                if (fileToChange.SharingLink == null)
                {
                    fileToChange.SharingLink = fileStorage.FileServer + Path.GetRandomFileName().Replace(".", "");
                    await db.SaveChangesAsync();
                }
                return Ok(fileToChange);
            }
            return BadRequest();
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            UserFile fileToDelete = await db.UserFiles.FirstOrDefaultAsync(file => file.Id == id);
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.Id == fileToDelete.UserId || user.isAdmin)
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), fileStorage.FilesDirectory, fileToDelete.ServerName);
                System.IO.File.Delete(path);
                db.UserFiles.Remove(fileToDelete);
                await db.SaveChangesAsync();
                return Ok(new { id });
            }
            return BadRequest();
        }

    }
}