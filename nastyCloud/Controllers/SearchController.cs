﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nastyCloud.Models;
using System.Threading.Tasks;
using nastyCloud.ViewModels;

namespace nastyCloud.Controllers
{
    public class SearchController : Controller
    {
        private CloudContext db;

        public SearchController(CloudContext context)
        {
            this.db = context;
        }

        [Authorize]
        public async Task<IActionResult> Index([FromQuery] string search)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            ViewBag.isAdmin = user.isAdmin;
            var files = search != null ? db.UserFiles.Where(file => file.ClientName.StartsWith(search)) : db.UserFiles;
            if (user.isAdmin)
            {
                var users = search != null ? db.Users.Where(searchUser => searchUser.Nickname.StartsWith(search)) : db.Users;
                var viewAdminSearch = new AdminSearchViewModel() { UserFiles = files.ToList(), Users = users.ToList() };
                return View(viewAdminSearch);
            }
            files = files.Where(file => file.isPublic);
            var viewUserSearch = new AdminSearchViewModel() { UserFiles = files.ToList(), Users = null };
            return View(viewUserSearch);
        }
    }
}