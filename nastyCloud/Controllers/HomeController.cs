﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nastyCloud.Models;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using System;
using Microsoft.AspNetCore.Http;

namespace nastyCloud.Controllers
{
    public class HomeController : Controller
    {
        private readonly CloudContext _db;

        public HomeController(
            CloudContext context
        )
        {
            _db = context;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            User user = await _db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            var files = _db.UserFiles.Where(file => file.UserId == user.Id).ToList();

            return View(files);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}