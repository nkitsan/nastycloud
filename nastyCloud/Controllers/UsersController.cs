﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nastyCloud.Models;

namespace nastyCloud.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class UsersController : Controller
    {
        private CloudContext db;

        public UsersController(CloudContext context)
        {
            this.db = context;
        }


        [Authorize]
        [HttpPut("{id}/rights")]
        public async Task<IActionResult> PutUserRights(int id)
        {
            User userIdentity = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (userIdentity.isAdmin)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
                user.isAdmin = user.isAdmin ? false : true;
                await db.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Id == id);
            User authorizedUser = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            if (user.Id != authorizedUser.Id && authorizedUser.isAdmin)
            {
                db.Users.Remove(user);
                await db.SaveChangesAsync();
                return Ok();
            }
            return BadRequest();
        }
    }
}