﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using nastyCloud.Models;

namespace nastyCloud.Controllers
{
    public class ChatController : Controller
    {
        private readonly CloudContext _db;

        public ChatController(CloudContext context)
        {
            _db = context;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            User user = await _db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);
            return View(user);
        }
    }
}
